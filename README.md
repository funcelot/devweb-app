# devweb.app

[![CircleCI](https://circleci.com/gh/funcelot/devweb.app/tree/master.svg?style=shield)](https://app.circleci.com/pipelines/github/funcelot/devweb.app?branch=master)

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=funcelot_devweb.app&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=funcelot_devweb.app)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=funcelot_devweb.app&metric=alert_status)](https://sonarcloud.io/dashboard?id=funcelot_devweb.app)

[![DeepSource](https://deepsource.io/gh/funcelot/devweb.app.svg/?label=active+issues&show_trend=true)](https://deepsource.io/gh/funcelot/devweb.app/?ref=repository-badge)
